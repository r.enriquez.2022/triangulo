#!/usr/bin/env pythoh3

'''
Program to produce a triangle with numbers
'''

import sys


def line(number: int):
    """Return a string corresponding to the line for number"""
    number: str=(number*f"{number}")
    return number

def triangle(number: int):
    """Return a string corresponding to the triangle for number"""
    for number in range(1, number+1):
        line(number)
        print(line(number))

def main():
    '''number: int = sys.argv[1]
    text = triangle(int(number))
    print(text)'''
    number = int(input("introduce un número: "))
    assert 0<number<10, ValueError
    triangle(number)

if __name__ == '__main__':
    main()

